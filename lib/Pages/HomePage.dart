import 'package:flutter/material.dart';
import 'package:yt_clone/Pages/Biblioteca.dart';
import 'package:yt_clone/Pages/EmAlta.dart';
import 'package:yt_clone/Pages/Inicio.dart';
import 'package:yt_clone/Pages/Inscricoes.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentIndex = 0;
  List _pages = [
    Inicio(),
    EmAlta(),
    Inscricoes(),
    Biblioteca(),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.grey),
        backgroundColor: Colors.white,
        title: Image.asset("images/youtube.png", scale: 8),
        actions: [
          IconButton(
            icon: Icon(Icons.videocam),
            onPressed: (){},
          ),
          IconButton(
            icon: Icon(Icons.search),
            onPressed: (){},
          ),
          IconButton(
            icon: Icon(Icons.account_circle),
            onPressed: (){},
          ),
        ],
      ),
      body: _pages[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        onTap: (_index){
          setState(() {
            _currentIndex = _index;
          });
        },
        // ou shifting
        type: BottomNavigationBarType.fixed,
        fixedColor: Colors.red,
        items: [
          BottomNavigationBarItem(
              //backgroundColor: Colors.red,
              label: "Inicio",
              icon: Icon(Icons.home)
          ),
          BottomNavigationBarItem(
              label: "Em Alta",
              icon: Icon(Icons.whatshot)
          ),
          BottomNavigationBarItem(
              label: "Inscrições",
              icon: Icon(Icons.subscriptions)
          ),
          BottomNavigationBarItem(
              label: "Biblioteca",
              icon: Icon(Icons.folder)
          ),
        ]
      ),
    );
  }
}
